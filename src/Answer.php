<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

use SebastianBergmann\Timer\Timer;

function answer($answer) : void
{
    echo \PHP_EOL;
    echo 'ANSWER:' . \PHP_EOL;
    echo $answer;
    echo \PHP_EOL;
    echo \PHP_EOL;
    echo 'in ' . Timer::secondsToTimeString(\microtime(true) - $_SERVER['REQUEST_TIME_FLOAT']);
    echo \PHP_EOL;
    echo \PHP_EOL;

    exit(0);
}
