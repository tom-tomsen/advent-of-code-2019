<?php

// used https://github.com/petertseng/adventofcode-rb-2019/blob/master/14_space_stoichiometry.rb
// for help

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$RECIPES = readRecipies($fileContent);

$ore = ore_to_make(['FUEL' => 1]);
answer($ore);

function ore_to_make(array $things, array $leftovers = [])
{
    global $RECIPES;

    if (\array_keys($things) === ['ORE']) {
        return $things['ORE'];
    }

    $next = [];

    foreach ($things as $thing => $amountNeeded) {
        if ('ORE' === $thing) {
            $next['ORE'] = ($next['ORE'] ?? 0) + $amountNeeded;

            continue;
        }

        if (\array_key_exists($thing, $leftovers)) {
            $useLeftover = \min($leftovers[$thing], $amountNeeded);
            $amountNeeded -= $useLeftover;
            $leftovers[$thing] -= $useLeftover;
        }

        $recipe = $RECIPES[$thing];
        $times = \ceil($amountNeeded / $recipe['produced']);

        $leftovers[$thing] = ($leftovers[$thing] ?? 0) + $recipe['produced'] * $times - $amountNeeded;

        foreach ($recipe['inputs'] as $input => $inputAmount) {
            $next[$input] = ($next[$input] ?? 0) + $inputAmount * $times;
        }
    }

    return ore_to_make($next, $leftovers);
}

function readRecipies(string $fileContent) : array
{
    $recipes = [];

    foreach (\explode(\PHP_EOL, $fileContent) as $line) {
        [$left, $right] = \explode(' => ', $line);
        [$outputAmount, $outputName] = \explode(' ', $right);

        $inputs = [];

        foreach (\explode(', ', $left) as $part) {
            [$inputAmount, $inputIncredient] = \explode(' ', $part);
            $inputs[$inputIncredient] = $inputAmount;
        }

        $recipes[$outputName] = [
            'produced' => $outputAmount,
            'inputs' => $inputs,
        ];
    }

    return $recipes;
}
