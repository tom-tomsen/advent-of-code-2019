<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$lines = \explode(\PHP_EOL, $fileContent);

$stars = [];

foreach ($lines as $y => $line) {
    for ($x = 0; \mb_strlen($line) > $x; ++$x) {
        if ('#' === $lines[$y][$x]) {
            $stars["{$x}-{$y}"] = [$x, $y];
        }
    }
}

$coordinates = [];

foreach ($stars as $center) {
    $normalizedStars = [];

    foreach ($stars as $relative) {
        if ($center === $relative) {
            continue;
        }

        [$nX, $nY] = normalizeVector(
            $relative[0] - $center[0],
            $relative[1] - $center[1]
        );
        $normalizedStars["{$nX}-{$nY}"] = 1;
    }

    $sum = \array_sum($normalizedStars);

    if ($sum > $max) {
        $max = $sum;
        $coordinates = $center;
    }
    $max = \max($max, $sum);
}

answer($max);

function normalizeVector($x, $y)
{
    $v = \sqrt($x * $x + $y * $y);

    return[$x / $v, $y / $v];
}
