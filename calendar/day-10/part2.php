<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$lines = \explode(\PHP_EOL, $fileContent);

$stars = [];

foreach ($lines as $y => $line) {
    for ($x = 0; \mb_strlen($line) > $x; ++$x) {
        if ('#' === $lines[$y][$x]) {
            $stars["{$x}-{$y}"] = [$x, $y];
        }
    }
}

$center = [8, 16];
$normalizedStars = [];

foreach ($stars as $relative) {
    if ($center === $relative) {
        continue;
    }

    $a = $relative[0] - $center[0];
    $b = $relative[1] - $center[1];
    [$nX, $nY] = normalizeVector($a, $b);
    $angle = angle([$a, $b]);

    $dist = \sqrt($a ** 2 + $b ** 2);

    if (!\array_key_exists((string) $angle, $normalizedStars)) {
        $normalizedStars[(string) $angle] = [
            'angle' => $angle,
            'stars' => [
                [
                    'angle' => (string) $angle,
                    'dist' => $dist,
                    'star' => $relative,
                ],
            ],
        ];
    } else {
        $normalizedStars[(string) $angle]['stars'][] = [
            'angle' => $angle,
            'dist' => $dist,
            'star' => $relative,
        ];
    }
}

foreach ($normalizedStars as &$star) {
    \usort($star['stars'], static function ($a, $b) {
        return $a['dist'] <=> $b['dist'];
    });
}

\usort($normalizedStars, static function ($a, $b) {
    return $a['angle'] <=> $b['angle'];
});

$i = 0;
$found = true;

$anser = 0;

while ($found) {
    $found = false;

    foreach ($normalizedStars as &$star) {
        if (0 < \count($star['stars'])) {
            $found = true;
            $xstar = \array_shift($star['stars']);

            $x = $xstar['star'][0];
            $y = $xstar['star'][1];
            $a = $xstar['angle'];
            $d = $xstar['dist'];
            $j = $i + 1;

            if (200 === ++$i) {
                $answer = $xstar['star'][0] * 100 + $xstar['star'][1];

                break 2;
            }
        }
    }
}

answer($answer);

function normalizeVector($x, $y)
{
    $v = \sqrt($x * $x + $y * $y);

    return [$x / $v, $y / $v];
}

function angle($v)
{
    return \M_PI - \atan2($v[0], $v[1]);
}

function dot($u, $v)
{
    return $u[0] * $v[0] + $u[1] * $v[1];
}
