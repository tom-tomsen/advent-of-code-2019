<?php

/**
 * review: foreach hadnt worked - why?
 */

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$planetDesc = \explode(\PHP_EOL, $fileContent);

$planets = [];
$idxx = $idxz = $idxy = '';

foreach ($planetDesc as $planetDesc) {
    [$x, $y, $z] = \sscanf($planetDesc, '<x=%d, y=%d, z=%d>');
    $planets[] = [
        'x' => $x,
        'y' => $y,
        'z' => $z,
        'v' => [
            'x' => 0,
            'y' => 0,
            'z' => 0,
        ],
    ];

    $idxx .= '.' . $x;
    $idxy .= '.' . $y;
    $idxz .= '.' . $z;
}

$firstX = $idxx;
$firstY = $idxy;
$firstZ = $idxz;

$lx = 0;
$ly = 0;
$lz = 0;

for ($it = 1; 0 === $lx || 0 === $ly || 0 === $lz; ++$it) {
    for ($i = 0; \count($planets) > $i; ++$i) {
        for ($j = 0; \count($planets) > $j; ++$j) {
            if ($i === $j) {
                continue;
            }

            $planets[$i]['v']['x'] += ($planets[$j]['x'] <=> $planets[$i]['x']);
            $planets[$i]['v']['y'] += ($planets[$j]['y'] <=> $planets[$i]['y']);
            $planets[$i]['v']['z'] += ($planets[$j]['z'] <=> $planets[$i]['z']);
        }
    }

    $idxx = $idxz = $idxy = '';

    foreach ($planets as $i => &$planet) {
        $planet['x'] += $planet['v']['x'];
        $planet['y'] += $planet['v']['y'];
        $planet['z'] += $planet['v']['z'];

        $idxx .= '.' . $planet['x'];
        $idxy .= '.' . $planet['y'];
        $idxz .= '.' . $planet['z'];
    }

    if (0 === $lx && $idxx === $firstX) {
        $lx = $it + 1;
    }

    if (0 === $ly && $idxy === $firstY) {
        $ly = $it + 1;
    }

    if (0 === $lz && $idxz === $firstZ) {
        $lz = $it + 1;
    }
}

answer(\gmp_lcm($lx, \gmp_lcm($ly, $lz)));
