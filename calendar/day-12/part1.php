<?php

/**
 * review: foreach hadnt worked - why?
 */

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$planetDesc = \explode(\PHP_EOL, $fileContent);

$planets = [];

foreach ($planetDesc as $planetDesc) {
    [$x, $y, $z] = \sscanf($planetDesc, '<x=%d, y=%d, z=%d>');
    $planets[] = [
        'x' => $x,
        'y' => $y,
        'z' => $z,
        'v' => [
            'x' => 0,
            'y' => 0,
            'z' => 0,
        ],
    ];
}

$energy = 0;

foreach (\range(1, 1000) as $it) {
    for ($i = 0; \count($planets) > $i; ++$i) {
        for ($j = 0; \count($planets) > $j; ++$j) {
            if ($i === $j) {
                continue;
            }

            $planets[$i]['v']['x'] += ($planets[$j]['x'] <=> $planets[$i]['x']);
            $planets[$i]['v']['y'] += ($planets[$j]['y'] <=> $planets[$i]['y']);
            $planets[$i]['v']['z'] += ($planets[$j]['z'] <=> $planets[$i]['z']);
        }
    }

    $energy = 0;

    foreach ($planets as $i => &$planet) {
        $planet['x'] += $planet['v']['x'];
        $planet['y'] += $planet['v']['y'];
        $planet['z'] += $planet['v']['z'];

        $energy +=
            (\abs($planet['x']) + \abs($planet['y']) + \abs($planet['z']))
            * (\abs($planet['v']['x']) + \abs($planet['v']['y']) + \abs($planet['v']['z']));
    }
}

answer($energy);
