<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$width = 25;
$height = 6;
$pixels = $width * $height;

$result = [];

for ($i = 0; $i < $pixels; ++$i) {
    for ($j = $i; \mb_strlen($fileContent) > $j; $j += $pixels) {
        if ('2' !== $fileContent[$j]) {
            $result[$i] = '1' === $fileContent[$j] ? 'X' : ' ';

            break;
        }
    }
}

$answer = '';

foreach (\array_chunk($result, $width) as $row) {
    $answer .= \implode('', $row) . \PHP_EOL;
}

answer($answer);
