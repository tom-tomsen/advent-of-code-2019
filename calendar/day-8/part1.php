<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$width = 25;
$height = 6;
$pixels = $width * $height;

$c = [
    '0' => 0,
    '1' => 0,
    '2' => 0,
];
$min = \PHP_INT_MAX;
$answer = 0;

for ($i = 0; \mb_strlen($fileContent) > $i; ++$i) {
    ++$c[$fileContent[$i]];

    if (($i % $pixels) === $pixels - 1) {
        if ($c['0'] < $min) {
            $min = $c['0'];
            $answer = $c['1'] * $c['2'];
        }

        $c = [
            '0' => 0,
            '1' => 0,
            '2' => 0,
        ];
    }
}

answer($answer);
