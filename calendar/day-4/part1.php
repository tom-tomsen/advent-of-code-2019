<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
[$min, $max] = \explode('-', \trim($fileContent));

$count = 0;

for ($i = $min; $i < $max; ++$i) {
    $numStr = (string) $i;
    $adjacent = false;
    $incr = true;

    for ($x = 1; \mb_strlen($numStr) > $x; ++$x) {
        $prev = $numStr[$x - 1];
        $curr = $numStr[$x];

        if ($prev === $curr) {
            $adjacent = true;
        } elseif ($prev > $curr) {
            $incr = false;

            break;
        }
    }

    if ($incr && $adjacent) {
        ++$count;
    }
}

answer($count);
