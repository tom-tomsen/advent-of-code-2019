<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$original = \explode(',', $fileContent);

for ($noun = 0; 99 >= $noun; ++$noun) {
    for ($verb = 0; 99 >= $verb; ++$verb) {
        $program = \json_decode(\json_encode($original));
        $program[1] = $noun;
        $program[2] = $verb;

        $i = 0;

        while (true) {
            $opcode = $program[$i];

            switch ($opcode) {
                case 99:
                    break 2;
                case 1: // add
                    $sum = $program[$program[++$i]] + $program[$program[++$i]];
                    $program[$program[++$i]] = $sum;

                break;
                case 2: // multiply
                    $sum = $program[$program[++$i]] * $program[$program[++$i]];
                    $program[$program[++$i]] = $sum;

                break;

                default:
                    die("fail on {$i}");
            }

            ++$i;
        }

        if (19690720 === $program[0]) {
            answer(100 * $noun + $verb);
        }
    }
}

answer('fail');
