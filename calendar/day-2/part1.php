<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$program = \explode(',', $fileContent);

$program[1] = 12;
$program[2] = 2;

$i = 0;

while (true) {
    $action = $program[$i];

    switch ($action) {
        case 99:
            break 2;
        case 1: // add
            $sum = $program[$program[++$i]] + $program[$program[++$i]];
            $program[$program[++$i]] = $sum;

        break;
        case 2: // multiply
            $sum = $program[$program[++$i]] * $program[$program[++$i]];
            $program[$program[++$i]] = $sum;

        break;

        default:
            die("fail on {$i}");
    }

    ++$i;
}

answer($program[0]);
