<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

\define('N', 1);
\define('S', 2);
\define('W', 3);
\define('E', 4);

\define('WALL', 0);
\define('FLOOR', 1);
\define('FINISH', 2);

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$program = \explode(',', $fileContent);

$fx = $fy = 0;
$x = 30;
$dx = 0;
$y = 30;
$dy = 0;

for ($i = 0; 100 > $i; ++$i) {
    for ($j = 0; 100 > $j; ++$j) {
        $board[$i][$j] = null;
    }
}
$board[$y][$x] = 'S';

$path = [];
$i = 0;
$handle = \fopen('php://stdin', 'rb');
run(
    $program,
    static function () use (&$x, &$y, &$board, &$dx, &$dy, &$path) {
        do {
            if (!isset($board[$y + 1][$x])) { // down
                $dy = 1;
                $dx = 0;

                // echo "SOUTH\n";
                return S;
            }

            if (!isset($board[$y - 1][$x])) { // up
                $dy = -1;
                $dx = 0;

                // echo "NORTH\n";
                return N;
            }

            if (!isset($board[$y][$x + 1])) { // right
                $dy = 0;
                $dx = 1;

                // echo "EAST\n";
                return E;
            }

            if (!isset($board[$y][$x - 1])) { // left
                $dy = 0;
                $dx = -1;

                // echo "WEST\n";
                return W;
            }

            // echo "BACKtrace\n";
            [$oldX, $oldY] = \array_pop($path);

            if (1 === $oldX - $x) {
//                    echo "EAST\n";
                $dx = 1;
                $dy = 0;

                return E;
            }

            if (-1 === $oldX - $x) {
                //echo "WEST\n";
                $dx = -1;
                $dy = 0;

                return W;
            }

            if (1 === $oldY - $y) {
                //echo "SOUTH\n";
                $dx = 0;
                $dy = 1;

                return S;
            }

            if (-1 === $oldY - $y) {
                //echo "NORTH\n";
                $dx = 0;
                $dy = -1;

                return N;
            }
            // var_dump($oldX - $x, $oldY - $y);
        } while (!empty($path));

        return null;
    },
    static function ($val) use (&$x, &$y, &$board, &$dx, &$dy, &$path, &$handle, &$fx, &$fy) : void {
        switch ($val) {
            case WALL:
                $board[$y + $dy][$x + $dx] = 'W';

            break;
            case FINISH:
                $fx = $x + $dx;
                $fy = $y + $dy;
                $board[$y + $dy][$x + $dx] = 'o';
                $x += $dx;
                $y += $dy;
                $path[] = [$x, $y];

            break;
            case FLOOR:
                $board[$y + $dy][$x + $dx] = ' ';
                $x += $dx;
                $y += $dy;
                $path[] = [$x, $y];

            break;
                answer(\count($path) + 1);

            break;

            default:
                die("unknown output {$val}");
        }
    }
);

$active = true;
$step = 0;

for ($i = 0; $active; $i++, $step++) {
    $active = false;

    $newBoard = \json_decode(\json_encode($board));

    for ($y = 0; \count($board) > $y; ++$y) {
        for ($x = 0; \count($board[$y]) > $x; ++$x) {
            if ('o' === $board[$y][$x]) {
                if (' ' === $board[$y - 1][$x]) {
                    $active = true;
                    $newBoard[$y - 1][$x] = 'o';
                }

                if (' ' === $board[$y + 1][$x]) {
                    $active = true;
                    $newBoard[$y + 1][$x] = 'o';
                }

                if (' ' === $board[$y][$x - 1]) {
                    $active = true;
                    $newBoard[$y][$x - 1] = 'o';
                }

                if (' ' === $board[$y][$x + 1]) {
                    $active = true;
                    $newBoard[$y][$x + 1] = 'o';
                }
            }
        }
    }

    $board = $newBoard;
}

answer($step - 1);

function print_board(array $board, $a, $b, $path) : void
{
    for ($y = 0; 50 > $y; ++$y) {
        for ($x = 0; 50 > $x; ++$x) {
            if ($a === $x && $b === $y) {
                echo 'D';
            } elseif (isset($board[$y][$x])) {
                echo $board[$y][$x];
            } else {
                echo '.';
            }
        }
        echo "\n";
    }

    // echo "" . json_encode($path) . "\n\n";
}

function run($program, $inputCallback, $outputCallback) : void
{
    $base = 0;
    $i = 0;

    while (true) {
        $instruction = (string) $program[$i];

        $opcode = \mb_substr($instruction, \mb_strlen($instruction) - 2);
        $parameterMode = '' . \mb_substr($instruction, 0, \mb_strlen($instruction) - 2);

        switch ((int) $opcode) {
            case 99:
                break 2;
            case 1: // add
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                $sum = $a + $b;

                set($program, ++$i, $parameterMode[0], $sum, $base);

            break;
            case 2: // multiply
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                $mul = $a * $b;

                set($program, ++$i, $parameterMode[0], $mul, $base);

            break;
            case 3: // input
                $input = \call_user_func($inputCallback);
                set($program, ++$i, $parameterMode, $input, $base);

            break;
            case 4: // output
                $parameterMode = \str_pad($parameterMode, 1, '0', \STR_PAD_LEFT);

                $val = get($program, ++$i, $parameterMode[0], $base);

                \call_user_func($outputCallback, $val);

            break;
            case 5: // jump-if-true
                $parameterMode = \str_pad($parameterMode, 2, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[1], $base);
                $b = get($program, ++$i, $parameterMode[0], $base);

                if (0 !== $a) {
                    $i = $b - 1;
                }

            break;
            case 6: // jump-if-false
                $parameterMode = \str_pad($parameterMode, 2, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[1], $base);
                $b = get($program, ++$i, $parameterMode[0], $base);

                if (0 === $a) {
                    $i = $b - 1;
                }

            break;
            case 7: // less than
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                if ($a < $b) {
                    set($program, ++$i, $parameterMode[0], 1, $base);
                } else {
                    set($program, ++$i, $parameterMode[0], 0, $base);
                }

            break;
            case 8: // equals
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                if ($a === $b) {
                    set($program, ++$i, $parameterMode[0], 1, $base);
                } else {
                    set($program, ++$i, $parameterMode[0], 0, $base);
                }

            break;
            case 9: // ajust relative base
                $parameterMode = \str_pad($parameterMode, 1, '0', \STR_PAD_LEFT);

                $base += get($program, ++$i, $parameterMode[0], $base);

            break;

            default:
                die("unknown opcode {$opcode}");
        }
        ++$i;
    }
}

function get($program, $i, $parameterMode, $base) : int
{
    switch ((int) $parameterMode) {
        case 0:
            return (int) $program[$program[$i]];

        break;
        case 1:
            return (int) $program[$i];

        break;
        case 2:
            return (int) $program[$program[$i] + $base];

        break;
    }

    die("GET: unknown parameterMode {$parameterMode}");
}

function set(&$program, $i, $parameterMode, $value, $base = 0) : void
{
    switch ((int) $parameterMode) {
        case 0:
            $i = $program[$i];

        break;
        case 1:
            $i = $i;

        break;
        case 2:
            $i = $program[$i] + $base;

        break;
    }

    $program[$i] = $value;
}
