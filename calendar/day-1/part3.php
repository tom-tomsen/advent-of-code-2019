<?php

/**
 * Lesson learned:
 *  - can handle large files
 *  - is slower than \file().
 */

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$sum = 0;

foreach (\readline(__DIR__ . '/big.txt') as $line) {
    $x = calculate($line);

    while (0 < $x) {
        $sum += $x;
        $x = calculate($x);
    }
}

answer($sum);

function calculate($n)
{
    return \floor($n / 3) - 2;
}

function readLine(string $path) : \Generator
{
    $file = new \SplFileObject($path, 'r');

    while (!$file->eof()) {
        yield $file->fgets();
    }
}
