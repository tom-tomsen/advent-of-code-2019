<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$sum = 0;

foreach (\file(__DIR__ . '/input.txt', \FILE_IGNORE_NEW_LINES) as $line) {
    $sum += \floor($line / 3) - 2;
}

answer($sum);
