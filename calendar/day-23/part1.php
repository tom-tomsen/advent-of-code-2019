<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$computerCount = 50;
$computerArray = [];

foreach (\range(0, $computerCount - 1) as $computerId) {
    $computer = new IntCode(\explode(',', $fileContent));
    $computer->addInput($computerId);

    $computerArray[$computerId] = $computer;
}

for ($i = 0; 2000 > $i; ++$i) {
    foreach (\range(0, $computerCount - 1) as $computerId) {
        $computer = $computerArray[$computerId];
        $dest = $computer->run();

        if (null === $dest) {
            continue;
        }

        $x = $computer->run();
        $y = $computer->run();

        if (255 === $dest) {
            answer($y);
        }

        $destComputer = $computerArray[$dest];
        $destComputer->addInput($x);
        $destComputer->addInput($y);
    }
}

answer('nothing');

final class IntCode
{
    private $mem;

    private $p;

    private $base;

    private $inputQueue;

    public function __construct(array $mem)
    {
        $this->mem = $mem;
        $this->p = 0;
        $this->base = 0;
        $this->inputQueue = [];
    }

    public function addInput(int $input) : void
    {
        $this->inputQueue[] = $input;
    }

    public function run() : ?int
    {
        while (true) {
            $instruction = (string) $this->mem[$this->p];

            $opcode = \mb_substr($instruction, \mb_strlen($instruction) - 2);
            $parameterMode = '' . \mb_substr($instruction, 0, \mb_strlen($instruction) - 2);

            switch ((int) $opcode) {
                case 99:
                    throw new \Exception('END OF PROGRAM');

                    return null; // END
                case 1: // add
                    $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                    ++$this->p;
                    $a = $this->get($parameterMode[2], $this->base);
                    ++$this->p;
                    $b = $this->get($parameterMode[1], $this->base);

                    $sum = $a + $b;

                    ++$this->p;
                    $this->set($parameterMode[0], $sum, $this->base);

                break;
                case 2: // multiply
                    $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                    ++$this->p;
                    $a = $this->get($parameterMode[2], $this->base);
                    ++$this->p;
                    $b = $this->get($parameterMode[1], $this->base);

                    $mul = $a * $b;

                    ++$this->p;
                    $this->set($parameterMode[0], $mul, $this->base);

                break;
                case 3: // input
                    $input = \array_shift($this->inputQueue);

                    if (null === $input) {
                        $input = -1;
                    }

                    ++$this->p;
                    $this->set($parameterMode, $input, $this->base);
                    ++$this->p;

                    return null;

                break;
                case 4: // output
                    $parameterMode = \str_pad($parameterMode, 1, '0', \STR_PAD_LEFT);

                    ++$this->p;
                    $val = $this->get($parameterMode[0], $this->base);
                    ++$this->p;

                    return $val;

                break;
                case 5: // jump-if-true
                    $parameterMode = \str_pad($parameterMode, 2, '0', \STR_PAD_LEFT);

                    ++$this->p;
                    $a = $this->get($parameterMode[1], $this->base);
                    ++$this->p;
                    $b = $this->get($parameterMode[0], $this->base);

                    if (0 !== $a) {
                        $this->p = $b - 1;
                    }

                break;
                case 6: // jump-if-false
                    $parameterMode = \str_pad($parameterMode, 2, '0', \STR_PAD_LEFT);

                    ++$this->p;
                    $a = $this->get($parameterMode[1], $this->base);
                    ++$this->p;
                    $b = $this->get($parameterMode[0], $this->base);

                    if (0 === $a) {
                        $this->p = $b - 1;
                    }

                break;
                case 7: // less than
                    $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                    ++$this->p;
                    $a = $this->get($parameterMode[2], $this->base);

                    ++$this->p;
                    $b = $this->get($parameterMode[1], $this->base);

                    if ($a < $b) {
                        ++$this->p;
                        $this->set($parameterMode[0], 1, $this->base);
                    } else {
                        ++$this->p;
                        $this->set($parameterMode[0], 0, $this->base);
                    }

                break;
                case 8: // equals
                    $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                    ++$this->p;
                    $a = $this->get($parameterMode[2], $this->base);
                    ++$this->p;
                    $b = $this->get($parameterMode[1], $this->base);

                    if ($a === $b) {
                        ++$this->p;
                        $this->set($parameterMode[0], 1, $this->base);
                    } else {
                        ++$this->p;
                        $this->set($parameterMode[0], 0, $this->base);
                    }

                break;
                case 9: // ajust relative base
                    $parameterMode = \str_pad($parameterMode, 1, '0', \STR_PAD_LEFT);

                    ++$this->p;
                    $this->base += $this->get($parameterMode[0], $this->base);

                break;

                default:
                    die("unknown opcode {$opcode}");
            }
            ++$this->p;
        }
    }

    private function get($parameterMode) : int
    {
        switch ((int) $parameterMode) {
            case 0:
                return (int) $this->mem[$this->mem[$this->p]];

            break;
            case 1:
                return (int) $this->mem[$this->p];

            break;
            case 2:
                return (int) $this->mem[$this->mem[$this->p] + $this->base];

            break;
        }

        die("GET: unknown parameterMode {$parameterMode}");
    }

    private function set($parameterMode, $value) : void
    {
        switch ((int) $parameterMode) {
            case 0:
                $x = $this->mem[$this->p];

            break;
            case 1:
                $x = $this->p;

            break;
            case 2:
                $x = $this->mem[$this->p] + $this->base;

            break;
        }
        $this->mem[$x] = $value;
    }
}
