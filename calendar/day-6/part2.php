<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$lines = \explode(\PHP_EOL, \trim($fileContent));

$universe = build_universe($lines);

$path = [];
search('YOU', $universe, $path);

answer(\count($path) - 1);

function search(string $active_node, array $universe, array &$path) : bool
{
    $surroundingPlanets = $universe[$active_node]['c'];

    if ($universe[$active_node]['p']) {
        \array_push($surroundingPlanets, $universe[$active_node]['p']);
    }

    foreach ($surroundingPlanets as $planet) {
        if ('SAN' === $planet) { // found target
            return true;
        }

        if (true === $universe[$planet]['v']) { // visited before
            continue;
        }

        $universe[$planet]['v'] = true;
        $found = search($planet, $universe, $path, $paths);

        if ($found) {
            $path[] = $planet;

            return true;
        }
    }

    return false;
}

function build_universe(array $description) : array
{
    $universe = [];

    foreach ($description as $line) {
        [$planet, $orbitalPlanet] = \explode(')', $line);

        if (!$universe[$planet]) {
            $universe[$planet] = ['p' => null, 'c' => [], 'v' => false];
        }
        $universe[$planet]['c'][] = $orbitalPlanet;

        if (!$universe[$orbitalPlanet]) {
            $universe[$orbitalPlanet] = ['p' => null, 'c' => [], 'v' => false];
        }
        $universe[$orbitalPlanet]['p'] = $planet;

        foreach ($description as $line2) {
            [$refPlanet, $refOrbitalPlanet] = \explode(')', $line);

            if ($refPlanet === $planet && $refOrbitalPlanet !== $orbitalPlanet) {
                $universe[$planet]['c'][] = $refOrbitalPlanet;
            }
        }
    }

    return $universe;
}
