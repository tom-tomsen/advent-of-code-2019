<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$lines = \explode(\PHP_EOL, \trim($fileContent));

$universe = build_universe($lines);

$count = 0;

foreach ($universe as $planet) {
    $count += universe_count($planet, $universe);
}

answer($count);

function universe_count(array $graph, array $universe) : int
{
    if (empty($graph)) {
        return 0;
    }

    $c = 0;

    foreach ($graph as $g) {
        $c += 1 + universe_count($universe[$g], $universe);
    }

    return $c;
}

function build_universe(array $description) : array
{
    $universe = [];

    foreach ($description as $line) {
        [$planet, $orbitalPlanet] = \explode(')', $line);

        if (!$universe[$planet]) {
            $universe[$planet] = [];
        }
        $universe[$planet][] = $orbitalPlanet;

        if (!$universe[$orbitalPlanet]) {
            $universe[$orbitalPlanet] = [];
        }

        foreach ($description as $line2) {
            [$refPlanet, $refOrbitalPlanet] = \explode(')', $line);

            if ($refPlanet === $planet && $refOrbitalPlanet !== $orbitalPlanet) {
                $universe[$planet][] = $refOrbitalPlanet;
            }
        }
    }

    return $universe;
}
