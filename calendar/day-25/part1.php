<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$program = \explode(',', $fileContent);

/*



                      X<5>6     X     9
                        ^       ^     ^
             3          4   >   7   > 8      X          X |>
             ^          ^                    ^          ^
           2<X>     <1><S  > 10          >  11 >  12 > 13


1 photons   XXX
2 coin
3 molten lava   XXX
4 invinite loop  - invinite loop
5x mutex
6 antenna
7 cake
8 escape pod  - escape
9x pointer
10 giant electro magnet   - cant move
11x tamborine
12x fuel cell
13 boulder


boulder > all
pointer < cake
pointer < coin


tamborie , fc,  p, mutex

9, 5, 11, 12

 */

$commands = [
    'north',
    'north',
    'take mutex',
    'south',
    'east',
    'east',
    'north',
    'take pointer',
    'south',
    'west',
    'west',
    'south',
    'east',
    'east',
    'take tambourine',
    'east',
    'take fuel cell',
    'east',
    'north',
    'drop fuel cell',
    'drop tambourine',
    'drop pointer',
    'drop mutex',
    'east',
];

$commandID = 0;
$charID = 0;
$automatic = static function () use (&$commands, &$commandID, &$charID) {
    if (!\array_key_exists($commandID, $commands)) {
        return;
    }

    $command = $commands[$commandID];

    if (\mb_strlen($command) > $charID) {
        $chr = $command[$charID++];
    } else {
        $chr = "\n";
        $charID = 0;
        ++$commandID;
    }

    return \ord($chr);
};

$i = 0;
$command = null;
$manuelInput = static function () use (&$command, &$i) {
    if ($command && \mb_strlen($command) === $i) {
        $command = null;
    }

    if (!$command) {
        $command = \readline();
        $command .= "\n";
        $i = 0;
    }

    if ($command) {
        $chr = $command[$i++];
        echo $chr;

        return \ord($chr);
    }

    return \ord("\n");
};

run(
    $program,
    $automatic,
    static function ($val) use (&$board, &$x, &$y) : void {
        if (0 <= $val && 127 > $val) {
            echo \chr($val);
        } else {
            answer($val);
        }
    }
);

answer('nothing');

function run($program, $inputCallback, $outputCallback) : void
{
    $base = 0;
    $i = 0;

    while (true) {
        $instruction = (string) $program[$i];

        $opcode = \mb_substr($instruction, \mb_strlen($instruction) - 2);
        $parameterMode = '' . \mb_substr($instruction, 0, \mb_strlen($instruction) - 2);

        switch ((int) $opcode) {
            case 99:
                break 2;
            case 1: // add
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                $sum = $a + $b;

                set($program, ++$i, $parameterMode[0], $sum, $base);

            break;
            case 2: // multiply
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                $mul = $a * $b;

                set($program, ++$i, $parameterMode[0], $mul, $base);

            break;
            case 3: // input
                $input = \call_user_func($inputCallback);
                set($program, ++$i, $parameterMode, $input, $base);

            break;
            case 4: // output
                $parameterMode = \str_pad($parameterMode, 1, '0', \STR_PAD_LEFT);

                $val = get($program, ++$i, $parameterMode[0], $base);

                \call_user_func($outputCallback, $val);

            break;
            case 5: // jump-if-true
                $parameterMode = \str_pad($parameterMode, 2, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[1], $base);
                $b = get($program, ++$i, $parameterMode[0], $base);

                if (0 !== $a) {
                    $i = $b - 1;
                }

            break;
            case 6: // jump-if-false
                $parameterMode = \str_pad($parameterMode, 2, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[1], $base);
                $b = get($program, ++$i, $parameterMode[0], $base);

                if (0 === $a) {
                    $i = $b - 1;
                }

            break;
            case 7: // less than
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                if ($a < $b) {
                    set($program, ++$i, $parameterMode[0], 1, $base);
                } else {
                    set($program, ++$i, $parameterMode[0], 0, $base);
                }

            break;
            case 8: // equals
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                if ($a === $b) {
                    set($program, ++$i, $parameterMode[0], 1, $base);
                } else {
                    set($program, ++$i, $parameterMode[0], 0, $base);
                }

            break;
            case 9: // ajust relative base
                $parameterMode = \str_pad($parameterMode, 1, '0', \STR_PAD_LEFT);

                $base += get($program, ++$i, $parameterMode[0], $base);

            break;

            default:
                die("unknown opcode {$opcode}");
        }
        ++$i;
    }
}

function get($program, $i, $parameterMode, $base) : int
{
    switch ((int) $parameterMode) {
        case 0:
            return (int) $program[$program[$i]];

        break;
        case 1:
            return (int) $program[$i];

        break;
        case 2:
            return (int) $program[$program[$i] + $base];

        break;
    }

    die("GET: unknown parameterMode {$parameterMode}");
}

function set(&$program, $i, $parameterMode, $value, $base = 0) : void
{
    switch ((int) $parameterMode) {
        case 0:
            $i = $program[$i];

        break;
        case 1:
            $i = $i;

        break;
        case 2:
            $i = $program[$i] + $base;

        break;
    }

    $program[$i] = $value;
}
