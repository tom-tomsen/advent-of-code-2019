<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

\define('DEBUG', 0);

\define('BLACK', 0);
\define('WHITE', 1);

\define('N', 0);
\define('E', 1);
\define('S', 2);
\define('W', 3);

\define('TURN_LEFT', 0);
\define('TURN_RIGHT', 1);

\define('REL_X', 59);
\define('REL_Y', 66);

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

// dir
// 0, -1 up
// 1, 0 right
// -1, 0 left
// 0, 1 down

$board = [
    REL_X . '-' . REL_Y => WHITE,
];
$minX = 0;
$minY = 0;
$robot = [
    'dir' => N,
    'pos' => [REL_X, REL_Y],
];
$i = 0;
run(
    \explode(',', $fileContent),
    static function () use (&$board, &$robot) {
        debug("WHAT COLOR @ base?\n");

        if (\array_key_exists($robot['pos'][0] . '-' . $robot['pos'][1], $board)) {
            debug('> ROBOT: ' . \json_encode($robot) . "\n");
            debug('> COLOR: ' . $board[$robot['pos'][0] . '-' . $robot['pos'][1]] . "\n");

            return $board[$robot['pos'][0] . '-' . $robot['pos'][1]];
        }
        debug("> 0\n");

        return BLACK;
    },
    static function ($val) use (&$i, &$robot, &$board, &$minX, &$minY) : void {
        if (0 === $i++ % 2) {
            debug("GOT COLOR: {$val}\n");
            // color
            $x = $robot['pos'][0];
            $y = $robot['pos'][1];

            $minX = \max($minX, (int) $x);
            $minY = \max($minY, (int) $y);
            $id = $x . '-' . $y;
            debug("SETTING BOARD @{$id}\n");
            $board[$id] = $val;
            debug('BOARD: ' . \json_encode($board) . "\n");
        } else {
            debug("GOT TURN {$val}\n");
            debug(\json_encode($robot) . "\n");
            debug("MOVE ROBOT\n");
            $dir = $robot['dir'];

            if (TURN_RIGHT === $val) {
                $dir = ($dir + 1) % 4;
            } else {
                $dir = (4 + $dir - 1) % 4;
            }
            $robot['dir'] = $dir;

            switch ($dir) {
                case N:
                    $robot['pos'][1]--;

                break;
                case E:
                    $robot['pos'][0]++;

                break;
                case S:
                    $robot['pos'][1]++;

                break;
                case W:
                    $robot['pos'][0]--;

                break;
            }

            debug(\json_encode($robot) . "\n");
        }
    }
);

for ($x = 0; 182 > $x; ++$x) {
    for ($y = 0; 169 > $y; ++$y) {
        echo WHITE === $board[$y . '-' . $x] ? 'X' : ' ';
    }
    echo \PHP_EOL;
}

answer(\count($board));

function run($program, $inputCallback, $outputCallback) : void
{
    $base = 0;
    $i = 0;

    while (true) {
        $instruction = (string) $program[$i];

        $opcode = \mb_substr($instruction, \mb_strlen($instruction) - 2);
        $parameterMode = '' . \mb_substr($instruction, 0, \mb_strlen($instruction) - 2);

        switch ((int) $opcode) {
            case 99:
                break 2;
            case 1: // add
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                $sum = $a + $b;

                set($program, ++$i, $parameterMode[0], $sum, $base);

            break;
            case 2: // multiply
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                $mul = $a * $b;

                set($program, ++$i, $parameterMode[0], $mul, $base);

            break;
            case 3: // input
                $input = \call_user_func($inputCallback);
                set($program, ++$i, $parameterMode, $input, $base);

            break;
            case 4: // output
                $parameterMode = \str_pad($parameterMode, 1, '0', \STR_PAD_LEFT);

                $val = get($program, ++$i, $parameterMode[0], $base);

                \call_user_func($outputCallback, $val);

            break;
            case 5: // jump-if-true
                $parameterMode = \str_pad($parameterMode, 2, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[1], $base);
                $b = get($program, ++$i, $parameterMode[0], $base);

                if (0 !== $a) {
                    $i = $b - 1;
                }

            break;
            case 6: // jump-if-false
                $parameterMode = \str_pad($parameterMode, 2, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[1], $base);
                $b = get($program, ++$i, $parameterMode[0], $base);

                if (0 === $a) {
                    $i = $b - 1;
                }

            break;
            case 7: // less than
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                if ($a < $b) {
                    set($program, ++$i, $parameterMode[0], 1, $base);
                } else {
                    set($program, ++$i, $parameterMode[0], 0, $base);
                }

            break;
            case 8: // equals
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                if ($a === $b) {
                    set($program, ++$i, $parameterMode[0], 1, $base);
                } else {
                    set($program, ++$i, $parameterMode[0], 0, $base);
                }

            break;
            case 9: // ajust relative base
                $parameterMode = \str_pad($parameterMode, 1, '0', \STR_PAD_LEFT);

                $base += get($program, ++$i, $parameterMode[0], $base);

            break;

            default:
                die("unknown opcode {$opcode}");
        }
        ++$i;
    }
}

function get($program, $i, $parameterMode, $base) : int
{
    switch ((int) $parameterMode) {
        case 0:
            return (int) $program[$program[$i]];

        break;
        case 1:
            return (int) $program[$i];

        break;
        case 2:
            return (int) $program[$program[$i] + $base];

        break;
    }

    die("GET: unknown parameterMode {$parameterMode}");
}

function set(&$program, $i, $parameterMode, $value, $base = 0) : void
{
    switch ((int) $parameterMode) {
        case 0:
            $i = $program[$i];

        break;
        case 1:
            $i = $i;

        break;
        case 2:
            $i = $program[$i] + $base;

        break;
    }

    $program[$i] = $value;
}

function debug(string $msg) : void
{
    if (DEBUG) {
        echo $msg;
    }
}
