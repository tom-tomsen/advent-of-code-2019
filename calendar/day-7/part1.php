<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$amps = [];
$inputs = [4, 3, 2, 1, 0];
$permutations = new \drupol\phpermutations\Generators\Permutations($inputs, \count($inputs));
$max = 0;

foreach ($permutations->generator() as $permutation) {
    $iA = $iB = $iC = $iD = $iE = 0;

    $programA = \explode(',', $fileContent);
    $programB = \explode(',', $fileContent);
    $programC = \explode(',', $fileContent);
    $programD = \explode(',', $fileContent);
    $programE = \explode(',', $fileContent);

    $inputA = [$permutation[0], 0];
    $inputB = [$permutation[1]];
    $inputC = [$permutation[2]];
    $inputD = [$permutation[3]];
    $inputE = [$permutation[4]];

    $inputB[] = run($programA, $iA, static function () use (&$inputA) {
        return \array_shift($inputA);
    });
    $inputC[] = run($programB, $iB, static function () use (&$inputB) {
        return \array_shift($inputB);
    });
    $inputD[] = run($programC, $iC, static function () use (&$inputC) {
        return \array_shift($inputC);
    });
    $inputE[] = run($programD, $iD, static function () use (&$inputD) {
        return \array_shift($inputD);
    });
    $inputA[] = run($programE, $iE, static function () use (&$inputE) {
        return \array_shift($inputE);
    });

    $v = \array_shift($inputA);
    $max = \max($max, $v);
}

answer($max);

function run(&$program, &$i, $inputCallback) : int
{
    $base = 0;

    while (true) {
        $instruction = (string) $program[$i];

        $opcode = \mb_substr($instruction, \mb_strlen($instruction) - 2);
        $parameterMode = '' . \mb_substr($instruction, 0, \mb_strlen($instruction) - 2);

        switch ((int) $opcode) {
            case 99:
                break 2;
            case 1: // add
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                $sum = $a + $b;

                set($program, ++$i, $parameterMode[0], $sum, $base);

            break;
            case 2: // multiply
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                $mul = $a * $b;

                set($program, ++$i, $parameterMode[0], $mul, $base);

            break;
            case 3: // input
                $input = \call_user_func($inputCallback);

                if (null !== $input) {
                    set($program, ++$i, $parameterMode, $input, $base);
                }

            break;
            case 4: // output
                $parameterMode = \str_pad($parameterMode, 1, '0', \STR_PAD_LEFT);

                $val = get($program, ++$i, $parameterMode[0], $base);

                return $val;

            break;
            case 5: // jump-if-true
                $parameterMode = \str_pad($parameterMode, 2, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[1], $base);
                $b = get($program, ++$i, $parameterMode[0], $base);

                if (0 !== $a) {
                    $i = $b - 1;
                }

            break;
            case 6: // jump-if-false
                $parameterMode = \str_pad($parameterMode, 2, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[1], $base);
                $b = get($program, ++$i, $parameterMode[0], $base);

                if (0 === $a) {
                    $i = $b - 1;
                }

            break;
            case 7: // less than
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                if ($a < $b) {
                    set($program, ++$i, $parameterMode[0], 1, $base);
                } else {
                    set($program, ++$i, $parameterMode[0], 0, $base);
                }

            break;
            case 8: // equals
                $parameterMode = \str_pad($parameterMode, 3, '0', \STR_PAD_LEFT);

                $a = get($program, ++$i, $parameterMode[2], $base);
                $b = get($program, ++$i, $parameterMode[1], $base);

                if ($a === $b) {
                    set($program, ++$i, $parameterMode[0], 1, $base);
                } else {
                    set($program, ++$i, $parameterMode[0], 0, $base);
                }

            break;
            case 9: // ajust relative base
                $parameterMode = \str_pad($parameterMode, 1, '0', \STR_PAD_LEFT);

                $base += get($program, ++$i, $parameterMode[0], $base);

            break;

            default:
                die("unknown opcode {$opcode}");
        }
        ++$i;
    }
}

function get($program, $i, $parameterMode, $base) : int
{
    switch ((int) $parameterMode) {
        case 0:
            return (int) $program[$program[$i]];

        break;
        case 1:
            return (int) $program[$i];

        break;
        case 2:
            return (int) $program[$program[$i] + $base];

        break;
    }

    die("GET: unknown parameterMode {$parameterMode}");
}

function set(&$program, $i, $parameterMode, $value, $base = 0) : void
{
    switch ((int) $parameterMode) {
        case 0:
            $i = $program[$i];

        break;
        case 1:
            $i = $i;

        break;
        case 2:
            $i = $program[$i] + $base;

        break;
    }

    $program[$i] = $value;
}
