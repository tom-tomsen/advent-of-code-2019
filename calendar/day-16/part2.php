<?php

// 54746640: too low
// 55078585: ok

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$inputSignalPattern = \file_get_contents($inputFile);

$msgOffset = (int) \mb_substr($inputSignalPattern, 0, 7);

$inputSignal = \str_pad(
    $inputSignalPattern,
    10_000 * \mb_strlen($inputSignalPattern),
    $inputSignalPattern
);
$inputSignalLen = \mb_strlen($inputSignal) - 1;

for ($i = 0; 100 > $i; ++$i) {
    $newInputSignal = $inputSignal;
    $sum = 0;

    for ($j = $inputSignalLen; $j >= $msgOffset; --$j) {
        $sum += $inputSignal[$j];
        $newInputSignal[$j] = $sum % 10;
    }
    $inputSignal = $newInputSignal;
}

answer(\mb_substr($inputSignal, $msgOffset, 8));
