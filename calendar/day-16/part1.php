<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$inputSignal = \file_get_contents($inputFile);

for ($v = 0; 100 > $v; ++$v) {
    $patternParts = [0, 1, 0, -1];

    $pRep = 0;
    $pRel = 1;

    for ($j = 0; \mb_strlen($inputSignal) > $j; ++$j) {
        ++$pRep;
        // var_dump($pattern);

        $x = 0;

        for ($i = 0; \mb_strlen($inputSignal) > $i; ++$i) {
            $n = (int) $inputSignal[$i];

            //echo "$n*" . $pattern[($i + $pRel) % count($pattern)] . " + ";
            $x += $n * $patternParts[(\floor(($i + $pRel) / $pRep)) % 4];
        }
        //echo " = " . abs($x % 10) . "\n";
        $inputSignal[$j] = \abs($x % 10);
    }
}

answer(\mb_substr($inputSignal, 0, 8));
