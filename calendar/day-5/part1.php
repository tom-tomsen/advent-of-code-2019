<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$original = \explode(',', $fileContent);

$input = 1;
/*
for ($noun = 0; 99 >= $noun; ++$noun) {
    for ($verb = 0; 99 >= $verb; ++$verb) {
        $program = \json_decode(\json_encode($original));
        $program[1] = $noun;
        $program[2] = $verb;
 */
        $i = 0;
        $program = $original;

        while (true) {
            $instruction = (string) $program[$i];

            $opcode = \mb_substr($instruction, \mb_strlen($instruction) - 2);
            $parameterMode = '0' . \mb_substr($instruction, 0, \mb_strlen($instruction) - 2);

            switch ((int) $opcode) {
                case 99:
                    break 2;
                case 1: // add
                    if (0 === ($parameterMode & 001)) {
                        $a = $program[$program[++$i]];
                    } else {
                        $a = $program[++$i];
                    }

                    if (0 === ($parameterMode & 010)) {
                        $b = $program[$program[++$i]];
                    } else {
                        $b = $program[++$i];
                    }

                    $sum = $a + $b;

                    if (0 === ($parameterMode & 100)) {
                        $program[$program[++$i]] = $sum;
                    } else {
                        $program[++$i] = $sum;
                    }

                break;
                case 2: // multiply
                    if (0 === ($parameterMode & 001)) {
                        $a = $program[$program[++$i]];
                    } else {
                        $a = $program[++$i];
                    }

                    if (0 === ($parameterMode & 010)) {
                        $b = $program[$program[++$i]];
                    } else {
                        $b = $program[++$i];
                    }

                    $sum = $a * $b;

                    if (0 === ($parameterMode & 100)) {
                        $program[$program[++$i]] = $sum;
                    } else {
                        $program[++$i] = $sum;
                    }

                break;
                case 3: // input
                    if (0 === ($parameterMode & 1)) {
                        $program[$program[++$i]] = $input;
                    } else {
                        $program[++$i] = $input;
                    }

                break;
                case 4: // output
                    if (0 === ($parameterMode & 1)) {
                        echo \PHP_EOL . 'OUTPUT: ' . $program[$program[++$i]];
                    } else {
                        echo \PHP_EOL . 'OUTPUT: ' . $program[++$i];
                    }

                break;

                default:
                    die("fail on {$i}");
            }
            ++$i;
        }
/*
        if (19690720 === $program[0]) {
            answer(100 * $noun + $verb);
        }
    }
}
 */
