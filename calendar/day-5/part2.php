<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$original = \explode(',', $fileContent);

$input = 5;
$i = 0;
$program = $original;

while (true) {
    $instruction = (string) $program[$i];

    $opcode = \mb_substr($instruction, \mb_strlen($instruction) - 2);
    $parameterMode = '0' . \mb_substr($instruction, 0, \mb_strlen($instruction) - 2);

    switch ((int) $opcode) {
        case 99:
            break 2;
        case 1: // add
            if (0 === ($parameterMode & 001)) {
                $a = $program[$program[++$i]];
            } else {
                $a = $program[++$i];
            }

            if (0 === ($parameterMode & 010)) {
                $b = $program[$program[++$i]];
            } else {
                $b = $program[++$i];
            }

            $sum = $a + $b;

            if (0 === ($parameterMode & 100)) {
                $program[$program[++$i]] = $sum;
            } else {
                $program[++$i] = $sum;
            }

        break;
        case 2: // multiply
            if (0 === ($parameterMode & 001)) {
                $a = $program[$program[++$i]];
            } else {
                $a = $program[++$i];
            }

            if (0 === ($parameterMode & 010)) {
                $b = $program[$program[++$i]];
            } else {
                $b = $program[++$i];
            }

            $sum = $a * $b;

            if (0 === ($parameterMode & 100)) {
                $program[$program[++$i]] = $sum;
            } else {
                $program[++$i] = $sum;
            }

        break;
        case 3: // input
            if (0 === ($parameterMode & 1)) {
                $program[$program[++$i]] = $input;
            } else {
                $program[++$i] = $input;
            }

        break;
        case 4: // output
            if (0 === ($parameterMode & 1)) {
                $c = $program[++$i];
                echo \PHP_EOL . 'OUTPUT: ' . $program[$c];
            } else {
                echo \PHP_EOL . 'OUTPUT: ' . $program[++$i];
            }

        break;
        case 5: // jump-if-true
            if (0 === ($parameterMode & 001)) {
                $a = $program[$program[++$i]];
            } else {
                $a = $program[++$i];
            }

            if (0 === ($parameterMode & 010)) {
                $b = $program[$program[++$i]];
            } else {
                $b = $program[++$i];
            }

            if (0 !== $a) {
                $i = $b - 1;
            }

        break;
        case 6: // jump-if-false
            if (0 === ($parameterMode & 001)) {
                $a = $program[$program[++$i]];
            } else {
                $a = $program[++$i];
            }

            if (0 === ($parameterMode & 010)) {
                $b = $program[$program[++$i]];
            } else {
                $b = $program[++$i];
            }

            if (0 === $a) {
                $i = $b - 1;
            }

        break;
        case 7: // less than
            if (0 === ($parameterMode & 001)) {
                $a = $program[$program[++$i]];
            } else {
                $a = $program[++$i];
            }

            if (0 === ($parameterMode & 010)) {
                $b = $program[$program[++$i]];
            } else {
                $b = $program[++$i];
            }

            if ($a < $b) {
                if (0 === ($parameterMode & 100)) {
                    $program[$program[++$i]] = 1;
                } else {
                    $program[++$i] = 1;
                }
            } else {
                if (0 === ($parameterMode & 100)) {
                    $program[$program[++$i]] = 0;
                } else {
                    $program[++$i] = 0;
                }
            }

        break;
        case 8: // equals
            if (0 === ($parameterMode & 001)) {
                $a = $program[$program[++$i]];
            } else {
                $a = $program[++$i];
            }

            if (0 === ($parameterMode & 010)) {
                $b = $program[$program[++$i]];
            } else {
                $b = $program[++$i];
            }

            if ($a === $b) {
                if (0 === ($parameterMode & 100)) {
                    $c = $program[++$i];
                    $program[$c] = 1;
                } else {
                    $program[++$i] = 1;
                }
            } else {
                if (0 === ($parameterMode & 100)) {
                    $program[$program[++$i]] = 0;
                } else {
                    $program[++$i] = 0;
                }
            }

        break;

        default:
            die("fail on {$i}");
    }
    ++$i;
}
echo \PHP_EOL . \PHP_EOL;
