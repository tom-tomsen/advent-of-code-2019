<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);
$wires = \explode(\PHP_EOL, \trim($fileContent));

$movement = [
    'L' => [-1, 0],
    'R' => [1, 0],
    'U' => [0, -1],
    'D' => [0, 1],
];

$gridA = [];
$directions = \explode(',', $wires[0]);
$pos = [0, 0];
$step = 0;

foreach ($directions as $direction) {
    $orientation = $direction[0];
    $length = \mb_substr($direction, 1);

    for ($i = $length; 0 < $i; --$i) {
        $pos[0] += $movement[$orientation][0];
        $pos[1] += $movement[$orientation][1];
        $gridA[$pos[0] . '_' . $pos[1]] = ++$step;
    }
}

$gridB = [];
$directions = \explode(',', $wires[1]);
$pos = [0, 0];
$step = 0;

foreach ($directions as $direction) {
    $orientation = $direction[0];
    $length = \mb_substr($direction, 1);

    for ($i = $length; 0 < $i; --$i) {
        $pos[0] += $movement[$orientation][0];
        $pos[1] += $movement[$orientation][1];
        $gridB[$pos[0] . '_' . $pos[1]] = ++$step;
    }
}

$min = \PHP_INT_MAX;

foreach ($gridA as $key => $g) {
    if (isset($gridB[$key])) {
        $min = \min($min, $g + $gridB[$key]);
    }
}

answer($min);
