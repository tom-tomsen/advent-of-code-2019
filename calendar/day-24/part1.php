<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

\define('BUG', '#');
\define('SPACE', '.');

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$grid = initGrid($fileContent);

$snapshots = [];
$nextGrid = $grid;
$i = 0;
$snapshot = snapshot($grid);

do {
    $snapshots[] = $snapshot;

    foreach ($grid as $rowID => $row) {
        foreach ($row as $colID => $cell) {
            $bugs = bugCount($grid, $rowID, $colID);

            $nextGrid[$rowID][$colID] = $cell;

            if (BUG === $cell && 1 !== $bugs) {
                $nextGrid[$rowID][$colID] = SPACE;
            } elseif (SPACE === $cell && (1 === $bugs || 2 === $bugs)) {
                $nextGrid[$rowID][$colID] = BUG;
            }
        }
    }

    $grid = $nextGrid;
    $snapshot = snapshot($grid);
} while (!\in_array($snapshot, $snapshots, true));

answer(biodiversityRating($grid));

function biodiversityRating(array $grid) : float
{
    $sum = 0.0;

    foreach ($grid as $rowID => $row) {
        foreach ($row as $colID => $cell) {
            if (BUG === $cell) {
                $sum += 2 ** ($rowID * 5 + $colID);
            }
        }
    }

    return $sum;
}

function bugCount(array $grid, int $row, int $col) : int
{
    $bugCount = 0;

    foreach (neighbors($grid, $row, $col) as $cell) {
        if (BUG === $cell) {
            ++$bugCount;
        }
    }

    return $bugCount;
}

function neighbors(array $grid, int $row, int $col) : \Generator
{
    if (0 < $row) {
        yield $grid[$row - 1][$col];
    }

    if (\count($grid) - 1 > $row) {
        yield $grid[$row + 1][$col];
    }

    if (0 < $col) {
        yield $grid[$row][$col - 1];
    }

    if (\count($grid[$row]) - 1 > $col) {
        yield $grid[$row][$col + 1];
    }
}

function snapshot(array $grid) : string
{
    return \md5(\json_encode($grid));
}

function print_grid(array $grid) : void
{
    echo \PHP_EOL;

    foreach ($grid as $rowID => $row) {
        foreach ($row as $colID => $cell) {
            echo $cell;
        }
        echo \PHP_EOL;
    }

    echo \PHP_EOL;
    echo \PHP_EOL;
}

function initGrid($fileContent) : array
{
    $grid = [];

    foreach (\explode(\PHP_EOL, $fileContent) as $rowID => $row) {
        $grid[$rowID] = [];

        foreach (\mb_str_split($row) as $colID => $cell) {
            $grid[$rowID][$colID] = $cell;
        }
    }

    return $grid;
}
