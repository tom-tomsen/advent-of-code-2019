<?php

declare(strict_types=1);

namespace tomtomsen\AdventOfCode2019;

require __DIR__ . '/../../vendor/autoload.php';

\define('BUG', '#');
\define('SPACE', '.');

$inputFile = __DIR__ . '/input.txt';
$fileContent = \file_get_contents($inputFile);

$baseGrid = initGrid($fileContent);

$grid = [
    0 => $baseGrid,
];

for ($minute = 0; 200 > $minute; ++$minute) {
    $grid = step($grid, $minute);
}

$totalBugCount = totalBugCount($grid);

answer($totalBugCount);

function step(array $grid, int $minute) : array
{
    $grid[0 - $minute - 1] = emptyGrid();
    $grid[0 - $minute - 2] = emptyGrid();
    $grid[$minute + 1] = emptyGrid();
    $grid[$minute + 2] = emptyGrid();

    $nextGrid = $grid;

    for ($depth = 0 - $minute - 1; $minute + 1 >= $depth; ++$depth) {
        foreach ($grid[$depth] as $rowID => $row) {
            foreach ($row as $colID => $cell) {
                $bugs = bugCount($grid, $depth, $rowID, $colID);

                $nextGrid[$depth][$rowID][$colID] = $cell;

                if (BUG === $cell && 1 !== $bugs) {
                    $nextGrid[$depth][$rowID][$colID] = SPACE;
                } elseif (SPACE === $cell && (1 === $bugs || 2 === $bugs)) {
                    $nextGrid[$depth][$rowID][$colID] = BUG;
                }
            }
        }
    }

    return $nextGrid;
}

function totalBugCount(array $grid) : int
{
    $count = 0;

    foreach ($grid as $depth => $gridLevel) {
        foreach ($gridLevel as $rowID => $row) {
            foreach ($row as $colID => $cell) {
                if (BUG === $cell) {
                    ++$count;
                }
            }
        }
    }

    return $count;
}

function bugCount(array $grid, int $depth, int $row, int $col) : int
{
    $bugCount = 0;

    foreach (neighbors($grid, $depth, $row, $col) as $cell) {
        if (BUG === $cell) {
            ++$bugCount;
        }
    }

    return $bugCount;
}

function neighbors(array $grid, int $depth, int $row, int $col) : \Generator
{
    //
    //   0 1 2 3 4
    // 0
    // 1
    // 2
    // 3
    // 4
    //

    switch ($row) {
        case 0:
            yield $grid[$depth - 1][1][2]; // top
            if (4 === $col) { // right
                yield $grid[$depth - 1][2][3];
            } else {
                yield $grid[$depth][$row][$col + 1];
            }

            yield $grid[$depth][$row + 1][$col]; // bottom
            if (0 === $col) { // left
                yield $grid[$depth - 1][2][1];
            } else {
                yield $grid[$depth][$row][$col - 1];
            }

        break;
        case 1:
            yield $grid[$depth][$row - 1][$col]; // top
            if (4 === $col) { // right
                yield $grid[$depth - 1][2][3];
            } else {
                yield $grid[$depth][$row][$col + 1];
            }

            if (2 === $col) { // bottom
                yield $grid[$depth + 1][0][0];

                yield $grid[$depth + 1][0][1];

                yield $grid[$depth + 1][0][2];

                yield $grid[$depth + 1][0][3];

                yield $grid[$depth + 1][0][4];
            } else {
                yield $grid[$depth][$row + 1][$col];
            }

            if (0 === $col) { // left
                yield $grid[$depth - 1][2][1];
            } else {
                yield $grid[$depth][$row][$col - 1];
            }

        break;
        case 2:
            yield $grid[$depth][$row - 1][$col]; // top
            if (1 === $col) { // right
                yield $grid[$depth + 1][0][0];

                yield $grid[$depth + 1][1][0];

                yield $grid[$depth + 1][2][0];

                yield $grid[$depth + 1][3][0];

                yield $grid[$depth + 1][4][0];
            } elseif (2 === $col) {
                return;
            } elseif (4 === $col) {
                yield $grid[$depth - 1][2][3];
            } else {
                yield $grid[$depth][$row][$col + 1];
            }

            yield $grid[$depth][$row + 1][$col]; // bottom
            if (3 === $col) { // left
                yield $grid[$depth + 1][0][4];

                yield $grid[$depth + 1][1][4];

                yield $grid[$depth + 1][2][4];

                yield $grid[$depth + 1][3][4];

                yield $grid[$depth + 1][4][4];
            } elseif (0 === $col) {
                yield $grid[$depth - 1][2][1];
            } else {
                yield $grid[$depth][$row][$col - 1];
            }

        break;
        case 3:
            if (2 === $col) { // top
                yield $grid[$depth + 1][4][0];

                yield $grid[$depth + 1][4][1];

                yield $grid[$depth + 1][4][2];

                yield $grid[$depth + 1][4][3];

                yield $grid[$depth + 1][4][4];
            } else {
                yield $grid[$depth][$row - 1][$col];
            }

            if (4 === $col) { // right
                yield $grid[$depth - 1][2][3];
            } else {
                yield $grid[$depth][$row][$col + 1];
            }

            yield $grid[$depth][$row + 1][$col]; // bottom

            if (0 === $col) { // left
                yield $grid[$depth - 1][2][1];
            } else {
                yield $grid[$depth][$row][$col - 1];
            }

        break;
        case 4:
            yield $grid[$depth][$row - 1][$col]; // top

            if (4 === $col) { // right
                yield $grid[$depth - 1][2][3];
            } else {
                yield $grid[$depth][$row][$col + 1];
            }

            yield $grid[$depth - 1][3][2]; // bottom

            if (0 === $col) { // left
                yield $grid[$depth - 1][2][1];
            } else {
                yield $grid[$depth][$row][$col - 1];
            }

        break;
    }
}

function print_grid(array $grid) : void
{
    echo \PHP_EOL;

    foreach ($grid as $depth => $levelGrid) {
        echo "DEPTH: {$depth}\n";

        foreach ($levelGrid as $rowID => $row) {
            foreach ($row as $colID => $cell) {
                echo $cell;
            }
            echo \PHP_EOL;
        }
    }

    echo \PHP_EOL;
    echo \PHP_EOL;
}

function emptyGrid()
{
    $grid = [];

    for ($rowID = 0; 5 > $rowID; ++$rowID) {
        $grid[$rowID] = [];

        for ($colID = 0; 5 > $colID; ++$colID) {
            $grid[$rowID][$colID] = SPACE;
        }
    }
    $grid[2][2] = '?';

    return $grid;
}

function initGrid($fileContent)
{
    $grid = [];

    foreach (\explode(\PHP_EOL, $fileContent) as $rowID => $row) {
        $grid[$rowID] = [];

        foreach (\mb_str_split($row) as $colID => $cell) {
            $grid[$rowID][$colID] = $cell;
        }
    }

    return $grid;
}
