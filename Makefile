up:: build
	docker run --rm -ti -v ${PWD}:${PWD} -w ${PWD} tomtomsen/advent-of-code-2019:latest /bin/bash

build::
	docker build --tag tomtomsen/advent-of-code-2019:latest .

composer-normalize:
	docker run --rm --volume=$(PWD):/check -w /check flintci/composer-normalize

editorconfig-checker:
	docker run --rm --volume=$(PWD):/check --volume=/tmp/composer:/root/.composer mstruebing/editorconfig-checker
