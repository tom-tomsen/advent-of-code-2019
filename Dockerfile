FROM php:7-cli-alpine

RUN apk add --no-cache \
    curl \
    wget \
    unzip \
    zip \
    git \
    bash \
    ncurses \
    gnupg \
&& \
#   gmp - GNU Multiple Precision
    apk add --no-cache \
    gmp-dev \
    && docker-php-ext-install gmp \
&& \
#   php ini
    cp ${PHP_INI_DIR}/php.ini-production ${PHP_INI_DIR}/php.ini && \
    sed -i 's/memory_limit = .*/memory_limit = -1/g' ${PHP_INI_DIR}/php.ini && \
    sed -i 's/error_reporting = .*/error_reporting = -1/g' ${PHP_INI_DIR}/php.ini && \
    sed -i 's/display_errors = .*/display_errors = On/g' ${PHP_INI_DIR}/php.ini && \
    sed -i 's/display_startup_errors = .*/display_startup_errors = On/g' ${PHP_INI_DIR}/php.ini
